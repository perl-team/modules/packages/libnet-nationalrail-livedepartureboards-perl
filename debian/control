Source: libnet-nationalrail-livedepartureboards-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper (>= 7)
Build-Depends-Indep: perl,
                     libsoap-lite-perl,
                     libtest-pod-coverage-perl,
                     libtest-pod-perl
Standards-Version: 3.8.4
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libnet-nationalrail-livedepartureboards-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libnet-nationalrail-livedepartureboards-perl.git
Homepage: https://metacpan.org/release/Net-NationalRail-LiveDepartureBoards

Package: libnet-nationalrail-livedepartureboards-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         libsoap-lite-perl
Description: Perl module for retrieving UK rail live departure board information
 Net::NationalRail::LiveDepartureBoards provides an interface to the
 National Rail Enquiries Live Departure Boards SOAP API.  This can be
 used to find the next trains arriving or departing a particular
 railway station in the UK.
 .
 Related modules on CPAN include WWW::LiveDepartureBoards and
 WWW::NationalRail, but those are implemented by scraping and parsing
 the relevant websites.  This SOAP API should be more stable.
